FROM node:8.14-alpine

RUN apk add --no-cache bash python build-base

WORKDIR /opt/iobroker/

RUN npm install iobroker --loglevel error --unsafe-perm
RUN npm install --production --unsafe-perm && find /opt/iobroker/iobroker-data/ -name "*.json" -exec sed -i "s/$(hostname)/<HOSTNAME>/g" {} \;

VOLUME /opt/iobroker/

EXPOSE 8081 8082 8083 8084

ADD docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]