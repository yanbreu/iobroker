#!/bin/sh

find /opt/iobroker/iobroker-data/ -name "*.json" -exec sed -i "s/<HOSTNAME>/$(hostname)/g" {} \;

node /opt/iobroker/node_modules/iobroker.js-controller/controller.js